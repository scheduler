struct complex_task {
	char * task_id;
	float phase;
	float period;
	float t_cpu;
	float deadline;
};

struct task {
	char * task_id;
	float t_cpu;
	float period;
};
