CC = gcc
TARGET = scheduler

SOURCE = main.c parser.c

all:
	$(CC) -o $(TARGET) $(SOURCE)

.PHONY: clean

clean:
	rm -f $(TARGET)
