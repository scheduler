#include <stdio.h>

#include "parser.h"
#include "main.h"

struct task * tasks;

int main(int argc, char ** argv) {
	printf("Initializing scheduler with file %s\n", argv[1]);
	tasks = parse(argv[1]);
}
