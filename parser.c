#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "main.h"

struct task * parse(char * file) {
	FILE * fd;
	char * line;
	char w[100];
	struct task * ts;
	regexp_t preg;
	regmatch_t pmatch
	
	fd = fopen(file, "r");
	if (fd == NULL) {
		char * err = malloc(strlen(program_name) + strlen(file) + 2);
		// Create a significative string for perror.
		sprintf(err, "%s: %s", program_name, file);
		perror(err);
		free(err);
		return ts;
	}
	do {
		line = fgets(w, 100, fd);
		if (line != NULL) {
			printf("%c!\n", line[0]);
		}
	} while (line != NULL);
	fclose(fd);
	return ts;
}
